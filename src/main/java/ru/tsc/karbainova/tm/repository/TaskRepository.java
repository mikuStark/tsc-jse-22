package ru.tsc.karbainova.tm.repository;

import ru.tsc.karbainova.tm.api.repository.ITaskRepository;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Override
    public boolean existsById(String userId, String id) {
        return findById(userId, id) != null;
    }

    @Override
    public Task findByIndex(String userId, int index) {
        final List<Task> entities = findAll(userId);
        return entities.get(index);
    }

    @Override
    public Task findByName(String userId, String name) {
        for (Task task : entities) {
            if (task == null) continue;
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeByName(String userId, String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public Task removeByIndex(String userId, int index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        remove(userId, task);
        return task;
    }

    @Override
    public Task taskUnbindById(String userId, String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(null);
        return task;
    }

    @Override
    public void removeAllTaskByProjectId(String userId, String projectId) {
        findAllTaskByProjectId(userId, projectId).forEach(o -> entities.remove(o.getId()));
    }

    @Override
    public Task bindTaskToProjectById(String userId, String projectId, String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public List<Task> findAllTaskByProjectId(String userId, String projectId) {
        return entities.stream().filter(o -> userId.equals(o.getUserId()) && projectId.equals(o.getProjectId()))
                .collect(Collectors.toList());
    }
}
