package ru.tsc.karbainova.tm.command.user;

import ru.tsc.karbainova.tm.command.UserAbstractCommand;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.util.TerminalUtil;

public class UserRemoveByLoginCommand extends UserAbstractCommand {
    @Override
    public String name() {
        return "user-remove-by-login";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return " Remove user";
    }

    @Override
    public void execute() {
        System.out.println("Enter login");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeByLogin(login);
    }

    @Override
    public Role[] roles() {
        return new Role[] {Role.ADMIN};
    }
}
