package ru.tsc.karbainova.tm.command.task;

import ru.tsc.karbainova.tm.command.TaskAbstractCommand;
import ru.tsc.karbainova.tm.enumerated.Role;

public class TaskClearCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "clear-task";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Clear task";
    }

    @Override
    public void execute() {
        String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CLEAR PROJECTS]");
        serviceLocator.getTaskService().clear(userId);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }
}
